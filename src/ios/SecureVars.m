/********* SecureVars.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>

@interface SecureVars : CDVPlugin {
  // Member variables go here.
}

- (void)getSecureVar:(CDVInvokedUrlCommand*)command;
@end

@implementation SecureVars

- (void)getSecureVar:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSNumber *deseada = [[command.arguments objectAtIndex:0] valueForKey:@"variable"];
    NSString *retorno;

    if (deseada != nil) {
        switch (deseada)
        {
            case 1:
                retorno = @"Variable deseada 1";
                break;
            case 2:
                retorno = @"Variable deseada 2";
                break;
            default:
                retorno = @"";
                break;
        }

        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:retorno];
    }
    else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }

    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
