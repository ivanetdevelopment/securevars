var exec = require('cordova/exec');

module.exports.getSecureVar = function(arg0, success, error) {
    exec(success, error, 'SecureVars', 'getSecureVar', [arg0]);
}